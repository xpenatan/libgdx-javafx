package application;/*
 * Copyright (c) 2002-2012 LWJGL Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * * Neither the name of 'LWJGL' nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import static javafx.collections.FXCollections.observableArrayList;
import static javafx.collections.FXCollections.observableList;
import static org.lwjgl.opengl.GL11.GL_VENDOR;
import static org.lwjgl.opengl.GL11.GL_VERSION;
import static org.lwjgl.opengl.GL11.glGetString;

import java.net.URL;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.util.StringConverter;

import org.lwjgl.util.stream.StreamHandler;
import org.lwjgl.util.stream.StreamUtil;
import org.lwjgl.util.stream.StreamUtil.RenderStreamFactory;
import org.lwjgl.util.stream.StreamUtil.TextureStreamFactory;

import com.badlogic.gdx.backends.lwjgl.LwjglFXGraphics;

/** The JavaFX application GUI controller. */
public class GUIController implements Initializable
{

	@FXML
	public AnchorPane gearsRoot;

	@FXML
	public ImageView imgView1;
	@FXML
	public ImageView imgView2;

	@FXML
	public Label fpsLabel;
	@FXML
	public Label fpsLabel2;
	@FXML
	public Label javaInfoLabel;
	@FXML
	public Label systemInfoLabel;
	@FXML
	public Label glInfoLabel;

	@FXML
	public CheckBox vsync;

	@FXML
	public ChoiceBox<RenderStreamFactory> renderChoice;
	@FXML
	public ChoiceBox<TextureStreamFactory> textureChoice;
	@FXML
	public ChoiceBox<BufferingChoice> bufferingChoice;

	@FXML
	public Slider msaaSamples;

	public LwjglFXGraphics graphics1;
	public LwjglFXGraphics graphics2;

	public GUIController()
	{
	}

	public void initialize(final URL url, final ResourceBundle resourceBundle)
	{

		//imgView1.fitWidthProperty().bind(gearsRoot.widthProperty());
		//imgView1.fitHeightProperty().bind(gearsRoot.heightProperty());

		final StringBuilder info = new StringBuilder(128);
		info.append(System.getProperty("java.vm.name")).append(' ').append(System.getProperty("java.version")).append(' ').append(System.getProperty("java.vm.version"));

		javaInfoLabel.setText(info.toString());

		info.setLength(0);
		info.append(System.getProperty("os.name")).append(" - JavaFX ").append(System.getProperty("javafx.runtime.version"));

		systemInfoLabel.setText(info.toString());

		bufferingChoice.setItems(observableArrayList(BufferingChoice.values()));

		msaaSamples.setMin(0);
		msaaSamples.setMax(0);
		if (System.getProperty("javafx.runtime.version").startsWith("2"))
			// The label formatter was not working until JavaFX 8.
			for (Node n : msaaSamples.getParent().getChildrenUnmodifiable())
			{
				if (!(n instanceof Label))
					continue;

				Label l = (Label) n;
				if ("MSAA Samples".equals(l.getText()))
				{
					l.setText("MSAA Samples (2^x)");
					break;
				}
			}
		else
			msaaSamples.setLabelFormatter(new StringConverter<Double>()
			{
				@Override
				public String toString(final Double object)
				{
					return Integer.toString(1 << object.intValue());
				}

				@Override
				public Double fromString(final String string)
				{
					return null;
				}
			});
	}

	// This method will run in the background rendering thread
	public void runGears()
	{

		final List<RenderStreamFactory> renderStreamFactories = StreamUtil.getRenderStreamImplementations();
		final List<TextureStreamFactory> textureStreamFactories = StreamUtil.getTextureStreamImplementations();

		final String vendor = glGetString(GL_VENDOR);
		final String version = glGetString(GL_VERSION);

		Platform.runLater(new Runnable()
		{
			public void run()
			{

				glInfoLabel.setText(vendor + " OpenGL " + version);

				renderChoice.setItems(observableList(renderStreamFactories));
				for (int i = 0; i < renderStreamFactories.size(); i++)
				{

					if (renderStreamFactories.get(i) == graphics1.getRenderStreamFactory())
					{

						renderChoice.getSelectionModel().select(i);
						break;
					}
				}
				renderChoice.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<RenderStreamFactory>()
				{
					public void changed(final ObservableValue<? extends RenderStreamFactory> observableValue, final RenderStreamFactory oldValue, final RenderStreamFactory newValue)
					{
						graphics1.setRenderStreamFactory(newValue);
						if (graphics2 != null)
							graphics2.setRenderStreamFactory(newValue);
					}
				});

				textureChoice.setItems(observableList(textureStreamFactories));
				for (int i = 0; i < textureStreamFactories.size(); i++)
				{
					if (graphics1 != null && textureStreamFactories.get(i) == graphics1.getTextureStreamFactory())
					{
						textureChoice.getSelectionModel().select(i);
						break;
					}
				}

				textureChoice.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TextureStreamFactory>()
				{
					public void changed(final ObservableValue<? extends TextureStreamFactory> observableValue, final TextureStreamFactory oldValue, final TextureStreamFactory newValue)
					{
						graphics1.setTextureStreamFactory(newValue);
						if (graphics2 != null)
							graphics2.setTextureStreamFactory(newValue);
					}
				});

				bufferingChoice.getSelectionModel().select(graphics1.getTransfersToBuffer() - 1);
				bufferingChoice.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<BufferingChoice>()
				{
					public void changed(final ObservableValue<? extends BufferingChoice> observableValue, final BufferingChoice oldValue, final BufferingChoice newValue)
					{
						graphics1.setTransfersToBuffer(newValue.getTransfersToBuffer());
						if (graphics2 != null)
							graphics2.setTransfersToBuffer(newValue.getTransfersToBuffer());
					}
				});

				vsync.selectedProperty().addListener(new ChangeListener<Boolean>()
				{
					public void changed(final ObservableValue<? extends Boolean> observableValue, final Boolean oldValue, final Boolean newValue)
					{
						graphics1.setVSync(newValue);
						if (graphics2 != null)
							graphics2.setVSync(newValue);
					}
				});

				final int maxSamples = graphics1.getMaxSamples();
				if (maxSamples == 1)
					msaaSamples.setDisable(true);
				else
				{
					msaaSamples.setMax(Integer.numberOfTrailingZeros(maxSamples));
					msaaSamples.valueProperty().addListener(new ChangeListener<Number>()
					{
						public void changed(final ObservableValue<? extends Number> observableValue, final Number oldValue, final Number newValue)
						{

							int samples = 1 << newValue.intValue();
							graphics1.setSamples(samples);
							if (graphics2 != null)
								graphics2.setSamples(samples);
						}
					});
				}
			}
		});

	}

	private enum BufferingChoice
	{
		SINGLE(1, "No buffering"), DOUBLE(2, "Double buffering"), TRIPLE(3,
				"Triple buffering");

		private final int transfersToBuffer;
		private final String description;

		private BufferingChoice(final int transfersToBuffer, final String description)
		{
			this.transfersToBuffer = transfersToBuffer;
			this.description = transfersToBuffer + "x - " + description;
		}

		public int getTransfersToBuffer()
		{
			return transfersToBuffer;
		}

		public String getDescription()
		{
			return description;
		}

		public String toString()
		{
			return description;
		}
	}

}